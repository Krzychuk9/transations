package pl.sda.banktransations;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Input {

    /**
     * print message to console and get input
     *
     * @param message print to console
     * @return string
     */
    public String getInputString(String message) {
        System.out.println(message);
        String input;
        Scanner sc = new Scanner(System.in);
        input = sc.nextLine();

        return input;
    }

    /**
     * print message to console and get input
     *
     * @param message print to console
     * @return int
     */
    public int getInputInt(String message) {
        System.out.println(message);
        int input = 0;
        do {
            try {
                Scanner sc = new Scanner(System.in);
                input = sc.nextInt();
                sc.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Podaj liczbę całkowitą!");
            }
        } while (input == 0);
        return input;
    }

    /**
     * print message to console and get input
     *
     * @param message print to console
     * @return double
     */
    public double getInputDouble(String message) {
        System.out.println(message);
        double input = 0.0;
        do {
            try {
                Scanner sc = new Scanner(System.in);
                input = sc.nextDouble();
                sc.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Podaj liczbę zmiennoprzecinkową!");
            }
        } while (input == 0.0);
        return input;
    }


}
