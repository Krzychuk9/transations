package pl.sda.banktransations;

import java.util.ArrayList;
import java.util.List;

public class Transations {
    private List<Transation> transations = new ArrayList<>();
    private final Customer customer;
    private double startBalance;
    private double endBalance;
    private Input in = new Input();

    /**
     * CONSTRUCTOR
     * GET INITIAL CUSTOMER DATA AND START BALANCE
     */
    public Transations() {
        this.customer = this.getCustomerInitial();
        this.startBalance = in.getInputDouble("Enter initial balance: ");
    }

    /**
     * creat customer obj from input
     *
     * @return customer
     */
    private Customer getCustomerInitial() {
        String name = in.getInputString("Enter customer name: ");
        String surname = in.getInputString("Enter customer surname: ");
        int age = in.getInputInt("Enter customer age: ");
        String adres = in.getInputString("Enter customer adres: ");
        return new Customer(name, surname, age, adres);
    }

    /**
     * adds new transation
     */
    public void addTransation() {
        String date = in.getInputString("Enter date of transation DD-MM-YY: ");
        TransType type = null;
        do {
            int option = in.getInputInt("Choose option 1.CASH 2. NON-CASH");
            switch (option) {
                case 1:
                    type = TransType.CASH;
                    break;
                case 2:
                    type = TransType.NON_CASH;
                    break;
            }
        } while (type == null);
        double price = in.getInputDouble("Enter price: ");
        transations.add(new Transation(date, type, price));
        this.setEndBalance();
    }

    /**
     * set end balance of all transations
     */
    private void setEndBalance() {
        double result = 0.0;
        for (Transation transation : transations) {
            result += transation.getPrice();
        }
        this.endBalance = startBalance - result;
    }

    @Override
    public String toString() {
        return "{\"transations\": " + "\"" + transations.toString() + "\"" + ",\"customer\": " + "\""
                + customer.toString() + "\"" + ",\"startBalance\": " + "\"" + startBalance + "\"" + ",\"endBalance\": "
                + "\"" + endBalance + "\"";
    }

    public List<Transation> getTransations() {
        return transations;
    }

    public double getStartBalance() {
        return startBalance;
    }

    public double getEndBalance() {
        return endBalance;
    }

    public Customer getCustomer() {
        return customer;
    }
}
