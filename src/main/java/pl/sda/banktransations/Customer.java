package pl.sda.banktransations;

public class Customer {
    private String name;
    private String surname;
    private int age;
    private String adres;

    /**
     * CONSTRUCTOR
     * @param name name of costumer
     * @param surname surname of costumer
     * @param age age of customer
     * @param adres adres of Customer
     */
    public Customer(String name, String surname, int age, String adres) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.adres = adres;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getAdres() {
        return adres;
    }

    @Override
    public String toString() {
        return "{\"name\": " + "\"" + name + "\"" + ",\"surname\": " + "\"" + surname + "\"" + ",\"age\": " + "\"" + age
                + "\"" + ",\"adres\": " + "\"" + adres + "\"";
    }
}
