package pl.sda.banktransations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JSONlogger {

    /**
     * save all data about customer and transations to json file
     * @param fileout name of file
     * @param element Transations obj
     * @throws IOException
     */
    public void saveToJSON(String fileout, Transations element) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter fw = new FileWriter(new File(fileout))) {
            String text = gson.toJson(element);
            fw.write(text);
            System.out.println("successfully!");
        }
    }

}
