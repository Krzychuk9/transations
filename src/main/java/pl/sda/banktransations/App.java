package pl.sda.banktransations;

import java.io.IOException;
import java.util.InputMismatchException;

public class App {
    private transient Input in = new Input();

    /**
     * starts app with initial menu in console
     */
    public void start() {
        System.out.println("Welcome!");
        Transations transations = new Transations();
        while (true) {
            try {
                int option = in.getInputInt("Choose option:\n1. Add transation \n2. Save to file \n3.Exit");
                switch (option) {
                    case 1:
                        transations.addTransation();
                        break;
                    case 2:
                        JSONlogger logger = new JSONlogger();
                        logger.saveToJSON(transations.getCustomer().getSurname() + ".json", transations);
                        break;
                    case 3:
                        System.out.println("Bye!");
                        return;
                }
            } catch (InputMismatchException e) {
                System.out.println("Wrong option");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
