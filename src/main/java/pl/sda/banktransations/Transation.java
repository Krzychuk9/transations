package pl.sda.banktransations;

public class Transation {
    private String date;
    private TransType type;
    private double price;

    /**
     * CONSTRUCTOR
     *
     * @param date  date of transation
     * @param type  cash or non-cash type
     * @param price of transation
     */
    public Transation(String date, TransType type, double price) {
        this.date = date;
        this.type = type;
        this.price = price;
    }

    @Override
    public String toString() {
        return "{\"date\": " + "\"" + date + "\"" + ",\"type\": " + "\"" + type + "\"" + ",\"price\": " + "\"" + price
                + "\"";
    }

    public String getDate() {
        return date;
    }

    public TransType getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }
}

